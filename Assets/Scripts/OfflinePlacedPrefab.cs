﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;


[RequireComponent(typeof(ARRaycastManager))]

public class OfflinePlacedPrefab : MonoBehaviour
{
    [SerializeField]
    private GameObject placedPrefab;

    public GameObject PlacedPrefab
    {
        get
        {
            return placedPrefab;
        }
        set
        {
            placedPrefab = value;
        }
    }
    private ARRaycastManager arRaycastManager;


    private void Awake()
    {
        arRaycastManager = GetComponent<ARRaycastManager>();

        for (int i = 0;  i < 10; i++)
        {
            Instantiate(placedPrefab, new Vector3(i * 0.2F, 0, 0), Quaternion.identity);
            Instantiate(placedPrefab, new Vector3(-i * 0.2F, 0, 0), Quaternion.identity);
            Instantiate(placedPrefab, new Vector3(0, i * 0.2F, 0), Quaternion.identity);
            Instantiate(placedPrefab, new Vector3(0, -i * 0.2F, 0), Quaternion.identity);
            Instantiate(placedPrefab, new Vector3(0,0,i * 0.2F), Quaternion.identity);
        }
    }
}

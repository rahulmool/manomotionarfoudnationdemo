﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]


public class HandGesture : MonoBehaviour
{
    private ARRaycastManager raycastManager;
    private TrackingInfo tracking;
    public Vector3 currentPosition;
    private GameObject spawnedObject;
    [SerializeField]
    private GameObject PlaceablePrefab;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
    private void Awake()
    {
        raycastManager = GetComponent<ARRaycastManager>();
    }

    

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        HandInfo detectedHand = ManomotionManager.Instance.Hand_infos[0].hand_info;

        if (Input.touchCount > 0
            || detectedHand.gesture_info.mano_gesture_trigger == ManoGestureTrigger.GRAB_GESTURE
            || detectedHand.gesture_info.mano_gesture_trigger == ManoGestureTrigger.CLICK
            || detectedHand.gesture_info.mano_gesture_trigger == ManoGestureTrigger.DROP
            || detectedHand.gesture_info.mano_gesture_trigger == ManoGestureTrigger.PICK
            || detectedHand.gesture_info.mano_gesture_trigger == ManoGestureTrigger.RELEASE_GESTURE
            )
        {
            tracking = ManomotionManager.Instance.Hand_infos[0].hand_info.tracking_info;
            currentPosition = Camera.main.ViewportToWorldPoint(new Vector3(tracking.palm_center.x, tracking.palm_center.y, tracking.depth_estimation));
            touchPosition =new Vector2( currentPosition.x,currentPosition.y);
            return true;
        }
        touchPosition = default;
        return false;
    }

    private void Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchPosition))
        {
            return;
        }
        else
        {
            Instantiate(PlaceablePrefab, currentPosition, Quaternion.identity);
        }
        if (raycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = s_Hits[0].pose;
            if (spawnedObject == null)
            {
                spawnedObject = Instantiate(PlaceablePrefab, hitPose.position, hitPose.rotation);
            }
            else
            {
                spawnedObject.transform.position = hitPose.position;
                spawnedObject.transform.rotation = hitPose.rotation;
            }

        }
    }

}
